#include "ControllerState.h"
#include <iostream>


int main() {

	double position = 5.0,
		velocity = 0,
		current = 1.0,
		mass = 1.0,
		gravity =9.80665,
		P=0.0, I=0.0, D=0.0;
		/*P=5.0,
		I=0.3,
		D=0.2;*/

	double deltaT = 0.01,
		systTime = 0,
		finalTime = 10,
		startingPosition =5.1;
label:
	std::cout << "Kindly enter P I D values and initialPosition separated by space" << std::endl;
	std::cin >> P >> I >> D;

	if ((P <= 0) || (I <= 0) || (D <= 0)) goto label;

	std::cout << "*********************************************************\n"
		<< "Parameters provided\n"
		<< "P \t" << P << std::endl
		<< "I \t" << I << std::endl
		<< "D \t" << D << std::endl
		<< "Starting Position \t" << startingPosition << std::endl
		<< "***************************************************************" << std::endl;

	//Create Parameters
	Params* params = initParams(mass,gravity,deltaT,position,current,P,I,D);

	//Create Controller
	ControllerState* controller = initController(startingPosition,current,params);

	while (systTime < finalTime) {
		ComputeNS(controller, params);
		UpdateCS(controller);
		PrintState(controller,systTime,params);

		systTime += deltaT; //  clock Up
	}

	return 0;
}