#pragma once

/*
	Data definition to hold all basic parameters required
	in simulation. 
	Ensures uniformity
*/
struct Params
{
	double mass;
	double gravity;
	double deltaT;
	double refPosition;
	double refCurrent;
	double refP;
	double refI;
	double refD;
	double k;
};

/*
	Data definition of basic parameters needed by the controller 
	for either Next state (NS) or Current state (CS)
*/
struct StateVariable {
	double position;
	double velocity;
	double acceleration;
	double error;
	double integralError;
	double derivError;
	double current;
};

/*
	Instantiate StateVariable for  CS and NS
*/
struct ControllerState {
	StateVariable CS;
	StateVariable NS;
};

/*
	Function: Initialize Parameters used in program and computes k
	Parameters: mass, gravity, deltaT, refPosition, refCurrent and the proportions for PID
	Return Value: pointer to params
*/
Params* initParams(double mass, double gravity, double deltaT, double refPosition, double refCurrent,double refP, double refI, double refD);

/*
	Function: Initialize the PID controller
	Parameters: position,velocity,current
	Return Value: pointer to controller state
*/
ControllerState* initController(double position, double current, Params * params);
/*
	Function: Calculate Next State of the controller
	Parameters: pointer to current state and time delta
	Return Value: None
*/
void ComputeNS(ControllerState* State, Params * params);

/*
	Function: Update State of the controller
	Parameters: pointer to state
	Return Value: None
*/
void UpdateCS(ControllerState* State);

/*
	Function: Display Controller State
	Parameters: Pointer to current State, current system time and parameter object
	Return Value: None
*/
void PrintState(ControllerState* State,double SysTime,Params* param);