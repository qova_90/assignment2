#include "ControllerState.h"
#include "EulersMethod.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

Params* initParams(double mass, double gravity, double deltaT, double refPosition, double refCurrent, double refP, double refI, double refD){
	/*
		Initialize new params with received mass, gravity deltaT and reference position and current
	*/
	double _k = mass * gravity * (refPosition * refPosition) / (refCurrent * refCurrent);
	Params * _params = new Params();
	_params->mass = mass;
	_params->gravity = gravity;
	_params->deltaT = deltaT;
	_params->refPosition = refPosition;
	_params->refCurrent = refCurrent;
	_params->refP = refP;
	_params->refI = refI;
	_params->refD = refD;
	_params->k = _k;

	return _params;
	//return new Params{ mass, gravity, deltaT, refPosition, refP,refI,refD,refCurrent,_k};
}

ControllerState* initController(double position, double current,Params * params) {
	/*
	Create new ControllerState structure
	Initialize current, position and velocity values of the system
	Calculate K
	calculate current acceleration
	calculate current error
	Initialize current integral and derivative error
	Set the parameters for each PID term
*/


	ControllerState* controller = new ControllerState();

	//Set initial values passed in
	controller->CS.position = position;
	controller->CS.velocity = 0;
	controller->CS.current = current ;
	

	//Initialize other values
	controller->CS.derivError = 0.0;
	controller->CS.integralError = 0.0;
	

	controller->NS.acceleration = 0.0;
	controller->NS.position = 0.0;
	controller->NS.velocity = 0.0;
	controller->NS.current = 0.0;
	controller->NS.derivError = 0.0;
	controller->NS.integralError = 0.0;
	controller->NS.error = 0.0;


	controller->CS.acceleration = -11.4015;

	controller->CS.error = params->refPosition - position;

	//Compute Next State Current
	controller->CS.current = (params->refCurrent) - ((params->refP * controller->CS.error) + (params->refI * controller->CS.integralError) + (params->refD * controller->CS.derivError));

	//Compute Next State Acceleration
	controller->CS.acceleration = ((params->mass * params->gravity) - (params->k * controller->CS.current * controller->CS.current) / (controller->CS.position * controller->CS.position)) / params->mass;

	return controller;
}


void ComputeNS(ControllerState* State, Params * params) {
	/*
	Use Euler method to compute next position
	User Euler method to compute next velocity
	Compute the next error
	Use Euler method to compute next integral error
	Compute the next derivate error
	Compute the next state current (proportional,integral,derivative summed together)
	Compute the next state acceleration
*/
	StateVariable CS = State->CS;
	StateVariable NS = State->NS;
	double deltaT = params->deltaT;

	NS.position = Euler(CS.position, CS.velocity, deltaT);
	NS.velocity = Euler(CS.velocity,CS.acceleration , deltaT); //Next Velocity

	NS.error =  params->refPosition - NS.position; //Next Error
	NS.integralError = Euler(CS.integralError, CS.error, deltaT); //Integral Error

	NS.derivError = (NS.error - CS.error) / deltaT; // Derivative Error

	//Compute Next State Current
	NS.current = (params->refCurrent) - ((params->refP * NS.error) + (params->refI * NS.integralError) + (params->refD * NS.derivError));

	//Compute Next State Acceleration
	NS.acceleration = ((params->mass * params->gravity) - (params->k * NS.current * NS.current) / (NS.position * NS.position)) / params->mass;

	State->NS = NS;
}

void UpdateCS(ControllerState* State) {
/*
		transfer next state values to current variables
*/
	State->CS = State->NS;
}


void PrintState(ControllerState* State, double sysTime,Params * param) {
	//create file object
	std::ofstream fileOutput;
	//Name output file by PID values separated by underscore
	std::string fileName = std::to_string(param->refP ) + "_" + std::to_string(param->refI) + "_" + std::to_string(param->refD);
	fileOutput.open("PID_" + fileName+".csv",std::ios::app);

	//At initial system time, provide heading to CSV file
	if (sysTime ==0) {
		std::string heading = "System time,current,position,velocity,acceleration,error,integralError,derivError\n";
		fileOutput << heading;
	}

	//Write to file
	std::string ouput = std::to_string(sysTime) + "," + std::to_string(State->CS.current) + "," + std::to_string(State->CS.position) + "," + std::to_string(State->CS.velocity) + "," +
		std::to_string(State->CS.acceleration) + "," + std::to_string(State->CS.error) + "," + std::to_string(State->CS.integralError) + "," + std::to_string(State->CS.derivError) + "\n";
	fileOutput << ouput;

	// Write to screen
	int w = 10;
	std::cout<< std::setw(w)<< sysTime <<std::setw(w) <<State->CS.current << std::setw(w+w)<<State->CS.position << std::setw(w+w)
			  << State->CS.velocity <<std::setw(w+w)<< State->CS.acceleration << std::endl;
}