#include "EulersMethod.h"

/*
  Returns an integral 
*/
double Euler(double x, double dxdt, double deltaT)

{
	return(x + deltaT * dxdt);
}

